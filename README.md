# Basic React Components - III

In this lesson we will continue learning using new react componnents and at the same time improve our ongoing app.

Main themes for this lesson are following

- Writing touchable or pressable component. So far we have list of 'Cards' displayed in the main screen but they are not pressable. We will make that happen on this lesson.
- Working with lists. Lists are frequently used components in any app. Depending on use cases, wne can write write own/custom list or use react native provided Lists components. In this session  we will see how some of the react native provided can be used!

## Preparations
If you do a fresh start of a repo, copy inft2508 folder from previous lesson (https://gitlab.com/sysdev-tutorials/react-native/04-basic-react-components-ii/-/tree/main/inft2508) to your repo!

Then we are good to go!


## Make 'Card' component touchable
There are couple of alternatives making components touchable. In this lesson we will use 'Pressable API' which is more extensive and future-proof way to handle touch-based interactions! For more detials on how it works, refer official documentation here https://reactnative.dev/docs/pressable.

In order to add pressable feature to a component, you need to do following
- import 'Pressable' component from react-native package
- then wrap a target component with 'Pressable' component
- and implement 'onPress' event handler function of 'Pressable' component

In the snippet below, an 'Alert' dialog will be shown with text 'Pressed' when any of the 'Card' component is pressed.

<img align="right" src="pressable_1.png" width=200>

```
// contents of Card.js file

import React from 'react';
import { 
  Text, 
  View, 
  Image,
  Pressable,
  Alert
 } from 'react-native';

const Card = (props) => {

  return (
    <Pressable onPress ={() => {Alert.alert("Pressed");}}>
      <View style={{ 
            backgroundColor: "#9FE8FF",
            height: 100,
            width: 150,
            borderRadius: 10,
            justifyContent: "center", 
            alignItems: "center",
            margin: 10,
          }}>
          <View style= {{flex: 1, justifyContent: "flex-end"}}>
            <Image
              style= {{width: 40, height: 40}}
              source={props.logo}
            />
          </View>
          <View style= {{flex: 1, justifyContent: "flex-start"}}>
            <Text>{props.displayText}</Text>
          </View>
      </View>
    </Pressable>
  );
}

export default Card;
```
<br clear="both">


### Writing reusable function
Note that in the snippet above, we have written an 'inline' function for 'onPress' event handler. This can be extracted into a separate reusable function like this

```
 ...
 const onCardPress = () => {
    Alert.alert("Pressed");
  }

  return (
    <Pressable onPress ={onCardPress}>
  ...  
```

Next, we will need to write proper logic inside 'onCardPress' function. The usecase for 'touch or press' is following.

>If an user presses on 'Food' card, list of restaurants with menu items will be shown. Similary list of songs can be shown upon click of 'Music' card, and so on for showing list of vechiles, list of properties etc.

That means, we need some data to show! Ideally, such data will be retrieved from remote server via Rest API or similar methods. Since we have not learned that yet in this course, we will start with local dummy dataset. In future lessons will improve this by using REST API.

### Prepare dummy dataset
Create a ```data``` folder under 'inft2508' folder and inside that create 'dummydata.json' file (note that folder name and file name can be any name!) Then fill the 'dummydata.json' file with following contents.

```
{
    "restaurants": [
        {
            "name": "Chinese Restaurent", 
            "address": "Prinsengata 1, 7010 Trondheim", 
            "data": [
                {"menu": "Pizza", "price": 50}, 
                {"menu": "Burger", "price": 25}, 
                {"menu": "Risotto", "price": 250}]
            },
        {
            "name": "Mexican Restaurent", 
            "address": "Kongensgata 10, 7020 Trondheim", 
            "data": [
                {"menu": "French Fries", "price": 250}, 
                {"menu":"Onion Rings", "price": 150}, 
                {"menu":"Fried Shrimps", "price": 20}]
        },
        {
            "name": "Indian Restaurent", 
            "address": "Munkegata 15, 7030 Trondheim",
            "data": [
                {"menu":"Water", "price": 15}, 
                {"menu": "Coke", "price": 150}, 
                {"menu": "Beer", "price": 50}]
        }
    ],
    "vechiles": [],
    "mobileDevices": [],
    "realstateProperties": [],
    "musicalItems": [],
    "profilesForDate": []
}
```

As you can see, the file contains data in json format, and only the 'restaurants' data is populated and the rest if the categories have empty data. Feel free to populate these data and test on your own. 

Now we have a dataset, and as mentioned before what we want is when an user presses on 'Food' card, list of restaurants with menu items will be shown, ```in a differnt UI that main UI```. We have not learnet about multipage app and page navigation yet! So, we do we do this? Next section has answer to this!


### Highlevel solution
In this lesson we will start with so call ```if...&&``` pattern! In the next (future lesson) we will improve this with multipage app pattern!

So, let´s start designing our solution!

On a high level, the solution will work like this: press on a particular 'Card' on the main page, the page contents will be replaced with new contents (or UI elements). For example on press of 'Food' card, the 'ScrollView' in the main page will be replaced with another compoent that will show list of restaurants!

<img src="solution_1.png" width=500>

### Implementation details for the solution
We are going to implement the above design in the following steps

<img align="right" src="conditional_render.png">

1. Define and use 'Callback' function.

    Define a function in 'App.js', in the main component, and this function will be used to update or change the UI. This function will be referred as ```callback function```. Then pass the callback function reference from main component to the 'Card' components via 'props'. Like below. Note the ```showItems``` property!

        ```
        <Card key={value.id} displayText={value.displayText} logo={value.logo} showItems={showCardItems}/>
        ```

2. Update state variables in callback function.

    When a press is detected on a 'Card' component, call the 'callback function'. When the callback function in the main component is executed, it gets proper data from 'dummydata.json' file (note that this may be replaced with API calls in future lessons) and then update state variables that will eventually rerender the main component. Like below

        ```
        // callback function from Card component
        const showCardItems =  (category) => {
        // read data
        var data = require('./data/dummydata.json');
        
        // update state
        if(category == 'Food') {
            setVisiblePage('items');
            setItems(data.restaurants);
        }}
        ```

3. Update rendering logic of the main component and using 'SectionList' react native component

    Use ```if...&&``` pattern to render or rerender the main component. For example by render the main UI will collection of 'Card' components by default or on initial start of the app. When the above callback function is called and state variables are updated, conditional rerendeting is done. For example if press of 'Food' card is detedted, list of restaurants are displayed and so on.

    You can see in the attached code below, a ```SectionList``` react native component is used for displaying list of restaurants. Note that this is just one possible solution. One can use other type or lists or even can design own custom component for display list of items.

    The ```SectionList``` react native component requires that these properties to be set!
     - sections: represents the list of data items
     - keyExtractor: represents how uniqueness of each list item is calculated
     - renderItem: represents how each list item will be rendered. In our case it is a 'View' component with two inner 'Text' components representing 'menu' and 'price'! Note that they layout configuration in main axis is ```justifyContent: "space-between"}```.
     - renderSectionHeader (optional): represents how the header for each list item will be rendered. In our case it is also a 'View' component with two inner 'Text' components representing 'Name' and 'address' of a restaurant item.
    

<br clear="both">


Complete (for far) code for ```Card.js``` and ```App.js``` files are following

```
// Contents of Card.js file
import React from 'react';
import { 
  Text, 
  View, 
  Image,
  Pressable,
 } from 'react-native';

const Card = (props) => {

  const onCardPress = () => {
   props.showItems(props.displayText)
  }
  
  return (
    <Pressable onPress ={onCardPress}>
      <View style={{ 
            backgroundColor: "#9FE8FF",
            height: 100,
            width: 150,
            borderRadius: 10,
            justifyContent: "center", 
            alignItems: "center",
            margin: 10,
          }}>
          <View style= {{flex: 1, justifyContent: "flex-end"}}>
            <Image
              style= {{width: 40, height: 40}}
              source={props.logo}
            />
          </View>
          <View style= {{flex: 1, justifyContent: "flex-start"}}>
            <Text>{props.displayText}</Text>
          </View>
      </View>
    </Pressable>
  );
}

export default Card;
```

```
// Contents of App.js file
import React, {useState} from 'react';
import { 
  View, 
  TextInput, 
  ScrollView, 
  SafeAreaView,
  SectionList,
  Text,
} from 'react-native';
import Card from './components/Card'

const Inft2508App = () => {
  const logos = {
      logo_music: require('./images/logo-music.png'),
      logo_vechiles: require('./images/logo-vechiles.png'),
      logo_food: require('./images/logo-food.png'),
      logo_realstate: require('./images/logo-realstate.jpeg'),
      logo_dating: require('./images/logo-dating.png'),
      logo_mobile: require('./images/logo-mobile.png'),
    };

    // initial state
    const initialState = [
      {id: 1, displayText: "Music", logo: logos.logo_music},
      {id: 2, displayText: "Vechiles", logo: logos.logo_vechiles},
      {id: 3, displayText: "Food", logo: logos.logo_food},
      {id: 4, displayText: "Real State", logo: logos.logo_realstate},
      {id: 5, displayText: "Dating", logo: logos.logo_dating},
      {id: 6, displayText: "Mobile", logo: logos.logo_mobile},
      {id: 7, displayText: "Music", logo: logos.logo_music},
      {id: 8, displayText: "Vechiles", logo: logos.logo_vechiles},
      {id: 9, displayText: "Food", logo: logos.logo_food},
      {id: 10, displayText: "Real State", logo: logos.logo_realstate},
      {id: 11, displayText: "Dating", logo: logos.logo_dating},
      {id: 12, displayText: "Mobile", logo: logos.logo_mobile},
      {id: 13, displayText: "Music", logo: logos.logo_music},
      {id: 14, displayText: "Vechiles", logo: logos.logo_vechiles},
      {id: 15, displayText: "Food", logo: logos.logo_food},
      {id: 16, displayText: "Real State", logo: logos.logo_realstate},
      {id: 17, displayText: "Dating", logo: logos.logo_dating},
      {id: 18, displayText: "Mobile", logo: logos.logo_mobile},
    ]

    // set initial states
    const [cards, setCards] = useState (initialState);
    const [visiblePage, setVisiblePage] = useState ("main");
    const [items, setItems] = useState ([]);


    // callback function from Card component
    const showCardItems =  (category) => {
      // read data
      var data = require('./data/dummydata.json');
    
      // update state
      if(category == 'Food') {
        setVisiblePage('items');
        setItems(data.restaurants);
      }
    }

  return (
    <SafeAreaView>
      <View style= {{ margin: 10, alignItems: "center" }}>
        <TextInput
          placeholder="Type here to search!"
          onChangeText={newText => {
            var matchedCards = [];
            // apply search only if search string length >= 2
            if(newText.trim().length >= 2) {
              initialState.map((value, index) => {
                if(value.displayText.includes(newText)) {
                  matchedCards.push(value);
                }
              });
              setCards(matchedCards);
            } else{
              setCards(initialState);
            }
          }}
        />
      </View>
      { visiblePage == 'main' &&
        <ScrollView>
          <View style={{ 
              flex: 1, 
              justifyContent: "center", 
              alignItems: "center",
              flexDirection: "row",
              flexWrap: "wrap",
              alignContent: "center",
              }}>

              {
                cards.map((value, index) => {
                    return <Card key={value.id} displayText={value.displayText} logo={value.logo} showItems={showCardItems}/>
                  }
                )
              }
          </View>
        </ScrollView>
      }
      { visiblePage == 'items' &&
        <View style={{
          paddingTop: 20}}>
            <SectionList 
              sections={items}
              keyExtractor={(item, index) => item + index}
              renderItem={({item}) => (
                  <View style={{
                    backgroundColor: "#D3FFF6", 
                    padding: 10, marginVertical: 2, 
                    flexDirection: "row", 
                    justifyContent: "space-between"}}>
                    <Text style={{fontSize: 18}}>{item.menu}</Text>
                    <Text style={{fontSize: 18}}>{item.price} nok</Text>
                  </View>
                )}
              renderSectionHeader={({ section: { name, address } }) => (
                <View>
                  <Text style={{fontSize: 24, backgroundColor: "#fff"}}>{name}</Text>
                  <Text style={{fontSize: 18, backgroundColor: "#fff"}}>{address}</Text>

                </View>
              )}
            />
        </View>
      }
    </SafeAreaView>
  );
}

export default Inft2508App;
```

## That´s for this lesson

So, this is the app looks like and behave so far! When a user clicks on for example a 'Food' Card, another UI with list of restaurants are shown. But note that there is no way to go back to the main/home screen. We wil fix that in the next lesson!

<image src="show_restuautants.png" width=400>

In the next lession, we will look in to making multipages in an application and navigation back and forth between them. 



