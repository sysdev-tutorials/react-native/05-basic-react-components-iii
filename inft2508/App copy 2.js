
import React from 'react';
import { Text, View, Image } from 'react-native';

const Inft2508App = () => {
  return (
    <View style={{ 
        flex: 1, 
        justifyContent: "center", 
        alignItems: "center",
}}>
      <View style={{ 
          backgroundColor: "#9FE8FF",
          height: 100,
          width: 150,
          borderRadius: 10,
          alignItems: "center",
          justifyContent: "center"
        }}>
            <Image
              style={{ flex: 1, width: 50, height: 50 }}
              source={require('./logo.png')}
            />
            <View style={{ flex: 1, justifyContent: "center"}}>
              <Text>Hello!</Text>
          </View>
      </View>
      
    </View>
  );
}

export default Inft2508App;